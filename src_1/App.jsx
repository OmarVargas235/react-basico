import { useState } from 'react';
import styled from 'styled-components';

import Card from './Card';
import SubMenu from './SubMenu';

const Container = styled.div`
    
`;

function App() {

    const [data, setData] = useState([
        {
            id: 1,
            color: '#21D0D1',
            options: [
                { title: 'title1', state: false },
                { title: 'title2', state: false },
                { title: 'title3', state: true },
            ],
            footer: 2,
        },
        {
            id: 2,
            color: '#FF7744',
            options: [
                { title: 'title4', state: false },
                { title: 'title5', state: false },
                { title: 'title6', state: true },
                { title: 'title7', state: true },
                { title: 'title8', state: true },
                { title: 'title9', state: true },
                { title: 'title10', state: true },
            ],
            footer: 1,
        },
    ]);

    const handleChange = (value, id) => {

        const copyData = JSON.parse(JSON.stringify(data));

        const match = copyData.find(el => el.id === id);
        
        const current = match.options.find(el => el.title === value);

        current.state = !current.state;

        setData(copyData);
    }

    const handleReset = (id) => {

        const copyData = JSON.parse(JSON.stringify(data));

        const match = copyData.find(el => el.id === id);
        const resetList = match.options.map(v => (v.state = false, v));
        match.options = resetList;

        setData(copyData);
    }
    
    return (
        <Container>
            <Card />

            { data.map((v, i) => <SubMenu key={i} {...v} onChange={handleChange} onReset={handleReset} />) }
        </Container>
    )
}

export default App;