import styled from "styled-components";

import Options, { Circle, Icon } from './options';
import Footer from './footer';

import check from '../images/check.jpg';
import cancel from '../images/cruz.png';

const Container = styled.div`
    margin-top: 15px;
    background-color: ${(props) => props.color};
    border-radius: 25px;
    padding: 10px;
    /* height: 200px; */
    width: 400px;
    padding-top: 15px;
    display: flex;
    flex-direction: column;
    justify-content: center;

    opacity: .8;
`;

const App = (params) => {

    const handleChange = (value, id) => {
        
        if (typeof params.onChange === 'function') params.onChange(value, id);
    }

    const handleReset = (id) => {

        if (typeof params.onReset === 'function') params.onReset(params.id);
    }

    return (
        <Container color={params.color}>
            {
                params.options.map((data, index) => (
                    <Options
                        key={index}
                        title={data.title}
                        state={data.state}
                        id={params.id}
                        onChange={handleChange}
                    />
                ))
            }

            {/* <Footer version={params.footer} onReset={} onCancel={} onCheck={} /> */}
            <Footer version={params.footer} onReset={handleReset} />
        </Container>
    );
}

export default App;