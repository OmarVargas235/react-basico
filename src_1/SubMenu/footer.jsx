import { useState } from 'react';
import styled from "styled-components";

import { Circle, Icon } from './options';

import reload from '../images/reload.jpg';
import check from '../images/check.jpg';
import cancel from '../images/cruz.png';

const Container = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
`;

const ContainerV1 = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
    margin-top: 15px;
    padding-top: 25px;
    padding-bottom: 25px;
    border-top: 1px solid lightgray;
`;

const Item = ({ v }) => {

    return (
        <Circle onClick={v.onclick} visible={true} style={{ marginLeft: '15px', height: '60px', width: '60px' }}>
            <Icon visible={true} src={v.icon} style={{ height: '60px', width: '60px' }} />
        </Circle>
    );
}

const App = ({ version, onReset, onCancel, onCheck }) => {

    const [] = useState();

    const handleReset = () => {

        if (typeof onReset === 'function') onReset();
    }

    const handleCheck = () => {

        if (typeof onReset === 'function') onCheck();

        console.log("check");
    }

    const handleCancel = () => {

        if (typeof onReset === 'function') onCancel();

        console.log("cancel");
    }

    switch (version) {

        case 1:

            let datav1 = [
                { icon: cancel, onclick: handleCancel },
                { icon: reload, onclick: handleReset },
                { icon: check, onclick: handleCheck }
            ];

            return (
                <ContainerV1>
                    {
                        datav1.map((v, i) => (
                            <Item key={i} v={v} />
                        ))
                    }
                </ContainerV1>
            );
        
        case 2:
            let data = [
                { icon: cancel, onclick: handleCancel },
                { icon: check, onclick: handleCheck }
            ];

            return (
                <Container>
                    {
                        data.map((v, i) => (
                            <Item key={i} v={v} />
                        ))
                    }
                </Container>
            );
        
        default:
            return <></>

    }
}

export default App;