import styled from "styled-components";

import check from '../images/check.jpg';

const Container = styled.div`
    margin-left: 25px;
    margin-top: 15px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin-right: 25px;
    align-items: center;
`;

const Title = styled.p`
    margin: 0;
    color: white;
    font-weight: 500;
    text-transform: uppercase;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
`;

export const Circle = styled.div`
    border-radius: 50%;
    border: 1.5px solid white;
    width: 35px;
    height: 35px;
    cursor: pointer;
    background-color: ${(props) => props.visible ? 'white' : 'transparent'};
`;

export const Icon = styled.img`
    height: 35px;
    width: 35px;
    object-fit: contain;
    border-radius: 50%;
    visibility: ${(props) => props.visible ? 'visible' : 'hidden'};
`;

const App = ({ title, state, id, onChange }) => {

    const handleClick = () => {

        if (typeof onChange === 'function') onChange(title, id);
    }

    return (
        <Container>
            <Title>{title}</Title>

            <Circle visible={state} onClick={handleClick}>
                <Icon src={check} alt="alt" visible={state} />
            </Circle>
        </Container>
    );
}

export default App;