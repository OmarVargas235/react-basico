import { useState } from 'react';
import styled from "styled-components";

export const Container = styled.div<{visible: boolean}>`
    border-radius: 50%;
    border: 1.5px solid white;
    width: 35px;
    height: 35px;
    cursor: pointer;
    background-color: ${(props) => props.visible ? 'white' : 'transparent'};
`;

export const Icon = styled.img<{visible: boolean}>`
    height: 35px;
    width: 35px;
    object-fit: contain;
    border-radius: 50%;
    visibility: ${(props) => props.visible ? 'visible' : 'hidden'};
`;

export interface params {
    image: string;
    onClick?: (value: boolean) => void;
}

const App = (params: params): JSX.Element => {

    const [visible, setVisible] = useState(false);

    const handleClick = () => {

        setVisible(!visible);

        if (typeof params.onClick === 'function') params.onClick(!visible);
    }

    return <Container visible={visible} onClick={handleClick}>
        <Icon src={params.image} alt="alt" visible={visible} />
    </Container>
}

export default App;