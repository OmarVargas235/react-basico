import styled from "styled-components";

import Check from './checkbox';
import check from '../images/check.jpg';

const Container = styled.div`
    margin-left: 25px;
    margin-top: 15px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin-right: 25px;
    align-items: center;
`;

const Title = styled.p`
    margin: 0;
    color: white;
    font-weight: 500;
    text-transform: uppercase;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
`;

export interface params {
    title: string;
    onClick?: (params: {value: string, state: boolean}) => void;
}

const App = (params: params): JSX.Element => {

    const handleClick = (state: boolean) => {

        if (typeof params.onClick === 'function') params.onClick({value: params.title, state });
    }

    return <Container>
        <Title>{params.title}</Title>

        <Check {...params} image={check} onClick={handleClick} />
    </Container>
}

export default App;