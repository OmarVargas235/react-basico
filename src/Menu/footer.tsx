import { useEffect, useState } from 'react';
import styled from "styled-components";

import Checkbox from './checkbox';

import reload from '../images/reload.jpg';
import check from '../images/check.jpg';
import cancel from '../images/cruz.png';

const Container = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
`;

const ContainerV1 = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
    margin-top: 15px;
    padding-top: 25px;
    padding-bottom: 25px;
    border-top: 1px solid lightgray;
`;

interface option {
    icon: string;
    onClick: () => void;
}

export interface params {
    enableReset: boolean;
    enableCancel: boolean;
}

const App = (): JSX.Element => {

    const [options, setOptions] = useState<options[]>([{ icon: check, onclick: handleCheck }, ]);

    useEffect(() => {

        setOptions([
            { icon: cancel, onclick: handleCancel },
            { icon: reload, onclick: handleReset },
            
        ]);

    }, []);

    const handleReset = () => {

        if (typeof onReset === 'function') onReset();
    }

    const handleCheck = () => {

        if (typeof onReset === 'function') onCheck();

        console.log("check");
    }

    const handleCancel = () => {

        if (typeof onReset === 'function') onCancel();

        console.log("cancel");
    }

    const data: option[] = [
        { icon: cancel, onClick: handleCancel },
        { icon: reload, onClick: handleReset },
        { icon: check, onClick: handleCheck }
    ];

    return <Container>
        {
            data.map((v, i) => (
                <Checkbox key={i} state={false} onClick={v.onClick} />
            ))
        }
    </Container>
}

export default App;