import { useState } from "react";
import styled from "styled-components";

import Option, { params } from './Menu/option';

const Container = styled.div`
    background-color: red;
`;

const App = (): JSX.Element => {

    const [data, setData] = useState<string[]>([
        'title1',
        'title2',
        'title3',
        'title4',
        'title5',
    ]);

    const handleClick: params['onClick'] = (e) => {

        
    }

    return <Container>
        {
            data.map((v, i) => (
                <Option key={i} title={v} onClick={handleClick} />
            ))
        }
    </Container>
}

export default App;